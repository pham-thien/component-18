var $bodyHtml = $('body,html');
var $body = $('body');
var breakPoint1 = 767;


/* ======================================
body fixed
====================================== */
var scrollPosi;

function bodyFix() {
	scrollPosi = $(window).scrollTop();
	$body.css({
		'position': 'fixed',
		'width': '100%',
		'z-index': '1',
		'top': -scrollPosi
	});
}

function bodyFixReset() {
	$body.css({
		'position': 'relative',
		'width': 'auto',
		'top': 'auto'
	});
	$bodyHtml.scrollTop(scrollPosi);
}

/* ======================================
icon open menu sp
====================================== */
$(function(){
	$('.c-icon1').on('click',function(){
		$(this).toggleClass('is-active');
		$(this).parent().siblings('.c-header__navi').toggleClass('is-active');
		if ($(this).hasClass('is-active')) {
			bodyFix();
		} else {
			bodyFixReset();
		}
	});
});

/* ======================================
to top
====================================== */
$(function(){
	var toTop = $('.c-top');
	$(window).scroll(function () {
		if ($( window ).width() > breakPoint1) {
			if ($(this).scrollTop() > 200) {
				toTop.fadeIn(500);
			} else {
				toTop.fadeOut(500);
			}
		}
	});
	$('.c-top a, .c-footer__toTop a').click(function (e) {
		e.preventDefault();
		$bodyHtml.animate({scrollTop: 0}, 500);
		return false;
	});
	$(window).on("load resize", function() {
		if ($(window).width() <= breakPoint1) {
			toTop.hide();
		}
	});
});

/* ======================================
slide)
====================================== */
$(document).on('ready', function() {
	$(".js-slide").slick({
		dots: false,
		arrows: false,
		fade: true,
		speed: 3000,
		autoplaySpeed: 3000,
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		focusOnSelect: false,
		pauseOnHover: false,
		accessibility: false
	});
});