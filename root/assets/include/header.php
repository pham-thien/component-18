<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
	<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<?php include('meta.php'); ?>
	<link href="/assets/js/slick/slick.css" rel="stylesheet">
	<link href="/assets/css/style.css" rel="stylesheet">
	<script src="/assets/js/jquery-3.3.1.min.js"></script>
	<script src="/assets/js/jquery-migrate-3.0.1.min.js"></script>
</head>
<body class="page-<?php echo $pageid; ?>">
	<header class="c-header">
		<div class="c-header__inner">
			<h1 class="c-header__logo">
				<a href="/">
					<img src="/assets/img/common/logo.png" width="240" height="33" alt="">
				</a>
			</h1>
			<div class="c-header__navi">
				<nav class="c-header__menu">
					<ul>
						<li><a href="#">お知らせ</a></li>
						<li><a href="#">当院について</a></li>
						<li><a href="#">矯正治療について</a></li>
						<li><a href="#">治療の流れ</a></li>
						<li><a href="#">料金</a></li>
						<li><a href="#">アクセス</a></li>
					</ul>
				</nav>
				<nav class="c-header__menuSP sp-only">
					<ul>
						<li>
							<a href="#">
								<img src="/assets/img/common/icon4.png" alt="" width="38" height="34">
								<span>インターネット<br>予約</span>
							</a>
						</li>
						<li>
							<a href="#">
								<img src="/assets/img/common/icon5.png" alt="" width="42" height="34">
								<span>電話で予約</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div class="c-btn1 pc-only">
				<a href="">インターネット予約	</a>
			</div>
			<div class="c-header__icon sp-only">
				<div class="c-icon1">
					<span class="c-icon1__line"></span>
					<span class="c-icon1__line"></span>
					<span class="c-icon1__line"></span>
				</div>
			</div>
		</div>
	</header>