<?php /*========================================
title
================================================*/ ?>
<div class="c-dev-title1">title</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (top)</div>
<div class="l-container">
	<div class="c-title1">
		<h2 class="c-title1__text">お知らせ</h2>
		<a class="c-title1__link" href="#">一覧を見る</a>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (access)</div>
<div class="c-title2">
	<h2 class="c-title2__text">アクセス</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (cost)</div>
<div class="c-title2">
	<h2 class="c-title2__text">料金</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (about)</div>
<div class="c-title2">
	<h2 class="c-title2__text">院内紹介</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (flow)</div>
<div class="c-title2">
	<h2 class="c-title2__text">矯正治療の流れ</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (privacy)</div>
<div class="c-title2">
	<h2 class="c-title2__text">個人情報について</h2>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-title1 (news)</div>
<div class="c-title2">
	<h2 class="c-title2__text">お知らせ</h2>
</div>