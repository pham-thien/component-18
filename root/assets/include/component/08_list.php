<?php /*========================================
list
================================================*/ ?>
<div class="c-dev-title1">list</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list1 (top)</div>
<div class="l-container">
	<ul class="c-list1">
		<li>
			<span>2016.00.00</span>
			<p>
				<a href="#">年末年始休診のお知らせ</a>
			</p>
		</li>
		<li>
			<span>2016.00.00</span>
			<p>
				<a href="#">0/0（月）、0/0（月）は研修のため、休診とさせていただきます。</a>
			</p>
		</li>
		<li>
			<span>2016.00.00</span>
			<p>
				<a href="#">市瀬矯正歯科サイト公開</a>
			</p>
		</li>
	</ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list2 (access)</div>
<div class="l-container">
	<ul class="c-list2">
		<li>
			<p class="c-list2__title">お支払いは、無利子・手数料なしの分割可能（銀行振込の場合、振込手数料をご負担いただきます。）</p>
			<p class="c-list2__text">一括または分割、医院窓口支払い、銀行振込、各種クレジットカード決算がお選びいただけます。（診断料の分割はできません）</p>
		</li>
		<li>
			<p class="c-list2__title">装置代・処置料・大人の矯正治療後の2年間のメンテナンス費を含んだ定額制</p>
			<p class="c-list2__text">来院毎のお支払いは不要ですので、お子様のみの通院も安心です。</p>
		</li>
		<li>
			<p class="c-list2__title">家族割引制度あり</p>
			<p class="c-list2__text">ご家族お2人以上から¥50,000の割引が受けられます。</p>
		</li>
		<li>
			<p class="c-list2__title">転居などにより当院に通院できなくなった場合、治療費の返還あり</p>
			<p class="c-list2__text">転居の可能性がある場合、分割払いを推奨しております。転居先での治療継続先をお探しいたします。</p>
		</li>
	</ul>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-list3 (news)</div>
<div class="l-container">
	<ul class="c-list3">
		<li>
			<span class="c-list3__tag">休診のお知らせ</span>
			<h3 class="c-list3__title">年末年始休診のお知らせ</h3>
			<p class="c-list3__time">2016.00.00</p>
			<div class="c-list3__text">
				<p>
					申し訳ありませんが、<br>
					12月29日（木）〜1月3日（火）までお休みをいただきます。<br>
					新年は１月4日から診療開始となります。<br>
					年末は12月29日まで、いつもの20時まで診療します。
				</p>
			</div>
		</li>
		<li>
			<span class="c-list3__tag">休診のお知らせ</span>
			<h3 class="c-list3__title">0/0（月）、0/0（月）は研修のため、休診とさせていただきます。</h3>
			<p class="c-list3__time">2016.00.00</p>
			<div class="c-list3__text">
				<p>
					申し訳ありませんが、<br>
					0/0（月）、0/0（月）は研修のため、休診とさせていただきます。
				</p>
			</div>
		</li>
		<li>
			<span class="c-list3__tag">その他</span>
			<h3 class="c-list3__title">市瀬矯正歯科サイト公開</h3>
			<p class="c-list3__time">2016.00.00</p>
			<div class="c-list3__text">
				<p>
					市瀬矯正歯科サイトを正式に公開いたしました。<br>
					どうぞ末永くよろしくお願いいたします。
				</p>
			</div>
		</li>
		<li>
			<span class="c-list3__tag">コラム</span>
			<h3 class="c-list3__title">ダミー</h3>
			<p class="c-list3__time">2016.00.00</p>
			<div class="c-list3__text">
				<p>
					ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
				</p>
			</div>
		</li>
		<li>
			<span class="c-list3__tag">その他</span>
			<h3 class="c-list3__title">ダミー</h3>
			<p class="c-list3__time">2016.00.00</p>
			<div class="c-list3__text">
				<p>
					ダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミーダミー
				</p>
			</div>
		</li>
	</ul>
</div>