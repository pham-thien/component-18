<?php /*========================================
other
================================================*/ ?>
<div class="c-dev-title1">other</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-mainvisual (top)</div>
<div class="c-mainvisual">
	<div class="c-mainvisual__image">
		<div class="js-slide">
			<div>
				<img src="/assets/img/top/slide1.jpg" alt="">
			</div>
			<div>
				<img src="/assets/img/top/slide2.jpg" alt="">
			</div>
			<div>
				<img src="/assets/img/top/slide3.jpg" alt="">
			</div>
		</div>
	</div>
	<div class="c-mainvisual__box">
		<img src="/assets/img/common/contact.png" alt="" width="244" height="244" class="pc-only">
		<img src="/assets/img/common/contact_sp.png" alt="" width="147" height="147" class="sp-only">
	</div>
	<span class="c-mainvisual__icon pc-only"></span>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-new (top)</div>
<div class="c-new">
	<div class="c-new__inner">
		<div class="c-new__title">
			<p>お知らせ</p>
		</div>
		<div class="c-new__info">
			<a href="">
				<span>2016.00.00</span>年末年始休診のお知らせ
			</a>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-note (access)</div>
<div class="l-container">
	<div class="c-note">
		<p><span>※</span>以下の場合、別途治療費がかかります。</p>
		<ul>
			<li>他院で行う治療費（虫歯や歯周病の治療・抜歯・つめ物やかぶせ物の作り直しなど）</li>
			<li>取り外しのできる装置の紛失による再制作費</li>
			<li>ご自身でお口の中の衛生管理ができないなどの患者さまのご都合で固定式装置を除去せざる得ない場合の費用（再装着前提）</li>
		</ul>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block1 (top)</div>
<section class="c-block1">
	<div class="l-container">
		<div class="c-title1">
			<h2 class="c-title1__text">お知らせ</h2>
			<a class="c-title1__link" href="#">一覧を見る</a>
		</div>
		<ul class="c-list1">
			<li>
				<a href="#">
					<span>2016.00.00</span>
					年末年始休診のお知らせ
				</a>
			</li>
			<li>
				<a href="#">
					<span>2016.00.00</span>
					0/0（月）、0/0（月）は研修のため、休診とさせていただきます。
				</a>
			</li>
			<li>
				<a href="#">
					<span>2016.00.00</span>
					市瀬矯正歯科サイト公開
				</a>
			</li>
		</ul>
	</div>
</section>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block2 pc-only (top)</div>
<div class="l-container">
	<div class="c-block2 pc-only">
		<div class="c-block2__inner">
			<div class="c-block2__title">
				<p>初診相談は無料で受付しております。ぜひご相談ください。</p>
			</div>
			<div class="c-block2__content">
				<div class="c-block2__col">
					<p class="c-block2__text">お電話でのご予約・お問い合わせ</p>
					<p class="c-block2__phone">045-947-3344</p>
				</div>
				<div class="c-block2__col">
					<p class="c-block2__text">24時間いつでも予約</p>
					<div class="c-btn1">
						<a href="">インターネット予約	</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block3 (access)</div>
<div class="c-block3">
	<div class="l-container">
		<div class="c-block3__logo">
			<img src="/assets/img/access/1.jpg" width="240" height="33" alt="">
		</div>
		<div class="c-block3__text">
			<p>〒221-0002 神奈川県横浜市神奈川区大口通35-33<br>Tel. 045-947-3344</p>
			<p>JR横浜線 大口駅 徒歩3分</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block4 (access)</div>
<div class="c-block4">
	<div class="l-container">
		<h2 class="c-block4__title">車でお越しの場合</h2>
		<div class="c-block4__image pc-only">
			<img src="/assets/img/access/2.png" width="980" height="450" alt="">
		</div>
		<div class="c-block4__image sp-only">
			<img src="/assets/img/access/3.png" width="690" height="565" alt="">
			<p>当院裏に駐車場3台を用意しております。<br>案内板に従って、裏側から入っていただくと便利です。</p>
			<img src="/assets/img/access/4.png" width="690" height="416" alt="">
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block5 (about)</div>
<div class="c-block5">
	<div class="c-block5__inner">
		<div class="c-block5__content">
			<div class="c-title2">
				<h2 class="c-title2__text">当院について</h2>
			</div>
			<div class="c-block5__text">
				<p>知ってますか？<br class="sp-only">あなたの笑顔は周りを幸せにします。<br>あなたとあなたの大切な人に、<br class="sp-only">誠実に真摯に向き合います。</p>
			</div>
			<div class="c-block5__text2">
				<p>申し訳ありませんが、当院では一般の歯科治療は行っておりません。当院は歯列矯正専門の歯科医院です。矯正専門医院として、より質の高い医療を患者様に提供していき、そのための研鑽を怠りません。</p>
				<p class="mb32">
					当院では患者様がベストな選択ができるように治療を複合的に検討し、治療期間やコストを考え、矯正治療に限らずインプラントや差し歯といった治療の優位点も検討の上、プランをご提示させていただきます。<br>
					またライフスタイルにあった治療のタイミングをご提案いたします。<br>
					むやみやたらに治療を勧めることもいたしません。
				</p>
				<p class="mb32">
					矯正治療は医療ですので必ず治療を行う上でのメリット・デメリットがあり、リスクも存在します。<br>
					そのため、当然耳障りのいいことばかりをお話しするわけではありません。<br>
					みなさまが納得出来るまでしっかりお話しさせていただき、治療にあたらせていただきます。
				</p>
				<p>その先にある笑顔のために。</p>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block6 (about)</div>
<div class="c-block6">
	<div class="c-block6__inner">
		<div class="c-title2">
			<h2 class="c-title2__text">医師紹介</h2>
		</div>
		<div class="c-block6__content">
			<div class="c-block6__text">
				<p>
					私の父は、この地で内科医として長年地域医療に貢献してきました。<br>
					引退をした父に代わって、今度は私が（業種は異なりますが、）矯正歯科の専門医として新たに地域の皆様に貢献したいと考えております。<br>
					内科医として父が常に言っていたことがあります。<br>
					「医者にとって一人の患者は多くの患者のうちの一人かもしれない。<br>
					でもその患者にとっては唯一人の医者であることを忘れてはならない。」<br>
					この言葉が僕の歯科医師としての根幹にあります。<br>
					一人一人の患者さまに誠実に真摯に向き合うことをお約束します。<br>
					一人一人の患者さまに（あなたとあなたの大切な人に）専門医として何ができるのか、とことん考えます。
				</p>
			</div>
			<div class="c-block6__text2">
				<p class="fs14">市瀬矯正歯科 院長 <span class="fs20">市瀬 毅</span></p>
				<p>
					松本歯科大学卒業後、<br>
					東京歯科大学歯科矯正学講座に在籍。<br>
					東京歯科大学水道橋病院矯正歯科、<br>
					神奈川県内の矯正歯科で研鑽を積み開業に至る。
				</p>
				<p>
					東京矯正歯科学会・日本矯正歯科学会 所属<br>
					日本矯正歯科学会認定医
				</p>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block7 (about)</div>
<div class="c-block7">
	<div class="c-block7__image">
		<img src="/assets/img/about/1.jpg" alt="">
	</div>
	<div class="c-block7__info">
		<div class="c-block7__inner">
			<h3 class="c-block7__title">待合室</h3>
			<p class="c-block7__text">予約制のため混雑しませんが、快適に過ごしていただくために、<br>テレビや雑誌もご用意しております。</p>
		</div>
	</div>
</div>
<div class="c-block7 is-reverse">
	<div class="c-block7__image">
		<img src="/assets/img/about/2.jpg" alt="">
	</div>
	<div class="c-block7__info">
		<div class="c-block7__inner">
			<h3 class="c-block7__title">キッズルーム</h3>
			<p class="c-block7__text">小さなお子様をお連れのお母さまが、安心して治療を<br class="pc-only">受けられるようにキッズルームもご用意しております。</p>
		</div>
	</div>
</div>
<div class="c-block7">
	<div class="c-block7__image">
		<img src="/assets/img/about/3.jpg" alt="">
	</div>
	<div class="c-block7__info">
		<div class="c-block7__inner">
			<h3 class="c-block7__title">バリアフリー</h3>
			<p class="c-block7__text">スロープの入口を設け、院内すべてがバリアフリーになっており<br class="pc-only">ますので、車いす、ベビーカーの方でも安心して治療を受けられ<br class="pc-only">ます。</p>
		</div>
	</div>
</div>
<div class="c-block7 is-reverse">
	<div class="c-block7__image">
		<img src="/assets/img/about/4.jpg" alt="">
	</div>
	<div class="c-block7__info">
		<div class="c-block7__inner">
			<h3 class="c-block7__title">診療室</h3>
			<p class="c-block7__text">予約調整により個室化も可能です。<br>他の患者さまが気になられるという方はお気軽にお申し付けください。</p>
		</div>
	</div>
</div>
<div class="c-block7">
	<div class="c-block7__image">
		<img src="/assets/img/about/5.jpg" alt="">
	</div>
	<div class="c-block7__info">
		<div class="c-block7__inner">
			<h3 class="c-block7__title">カウンセリングルーム</h3>
			<p class="c-block7__text">患者さまのベストな選択ができるように、不安や心配事を解消し、<br>
			治療方針を提案させていただきます。</p>
		</div>
	</div>
</div>
<div class="c-block7 is-reverse">
	<div class="c-block7__image">
		<img src="/assets/img/about/6.jpg" alt="">
	</div>
	<div class="c-block7__info">
		<div class="c-block7__inner">
			<h3 class="c-block7__title">CT完備</h3>
			<p class="c-block7__text">立体的に顎の骨格・歯並びを精密に調べられる歯科用CTを完備<br class="pc-only">し、患者さまに無駄な負担をかけず安心して治療を受けていただ<br class="pc-only">けます。</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block8 (flow)</div>
<div class="l-container">
	<div class="c-block8">
		<div class="c-block8__box">
			<div class="c-block8__info">
				<h3 class="c-block8__title">初診相談</h3>
				<p class="c-block8__time">約45分から1時間</p>
				<p class="c-block8__text">
					初診相談のご予約を入れて頂き、当院へお越しください。<br>
					問診票にご記入いただいた後、お顔とお口の中の写真をとらせていただきます。（写真を希望されない場合はお申し付けください。）<br>
					写真を見ながらお悩みや疑問、不安などを伺い、予測される治療方法、治療期間、費用等の説明を致します。
				</p>
			</div>
			<div class="c-block8__image">
				<img src="/assets/img/flow/1.jpg" width="400" height="240" alt="">
			</div>
			<div class="c-block8__box2 pc-only">
				<div class="c-block8__contact">
					<div class="c-block8__col1">
						<p>初診相談は無料で受付しております。<br>ぜひご相談ください。</p>
					</div>
					<div class="c-block8__col2">
						<p class="c-block8__text2">お電話でのご予約・お問い合わせ</p>
						<p class="c-block8__phone">045-947-3344</p>
					</div>
					<div class="c-block8__col3">
						<p class="c-block8__text2">24時間いつでも予約</p>
						<div class="c-btn1">
							<a href="">インターネット予約	</a>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="c-block8__box">
			<div class="c-block8__info">
				<h3 class="c-block8__title">精密検査</h3>
				<p class="c-block8__time">約30分から45分</p>
				<p class="c-block8__text">治療計画を立案するために、口腔内審査、レントゲン写真（必要であれば歯科用CTも追加致します）、歯型、噛み合わせの検査を行います。</p>
			</div>
			<div class="c-block8__image">
				<img src="/assets/img/flow/2.jpg" width="400" height="240" alt="">
			</div>
		</div>

		<div class="c-block8__box">
			<div class="c-block8__info">
				<h3 class="c-block8__title">診断</h3>
				<p class="c-block8__time">約1時間</p>
				<p class="c-block8__text">検査資料をもとに、現状を把握し、問題点と治療方針を明確にします。患者様のご希望を踏まえ使用する装置の選択、メリットやデメリット、費用や期間などをご説明し、ご同意をいただければ治療開始となります。</p>
			</div>
			<div class="c-block8__image">
				<img src="/assets/img/flow/3.jpg" width="400" height="240" alt="">
			</div>
		</div>

		<div class="c-block8__box">
			<div class="c-block8__info">
				<h3 class="c-block8__title">治療前の準備</h3>
				<p class="c-block8__time">約45分</p>
				<p class="c-block8__text">装置を使用することで多かれ少なかれお口の清掃が難しくなります。<br>
				その前に徹底したブラッシング指導、歯のクリーニングのためのお時間をとらせていただきます。</p>
			</div>
			<div class="c-block8__image">
				<img src="/assets/img/flow/4.jpg" width="400" height="240" alt="">
			</div>
		</div>

		<div class="c-block8__box">
			<div class="c-block8__info">
				<h3 class="c-block8__title">治療開始</h3>
				<p class="c-block8__time">1回約30分から1時間</p>
				<p class="c-block8__text">
					治療計画に基づき装置を装着し、治療を開始していきます。<br>
					通院間隔は通常4週間に一度ですが、お子様や装置の種類によって6〜8週間に一度の通院になることもあります。治療を開始してからも徹底したブラッシング指導とクリーニングを続けていきます。<br>
					定期的な通院ができない場合、治療期間に大きく影響しますので通院間隔を守りましょう。
				</p>
			</div>
			<div class="c-block8__image">
				<img src="/assets/img/flow/5.jpg" width="400" height="240" alt="">
			</div>
		</div>

		<div class="c-block8__box">
			<div class="c-block8__info">
				<h3 class="c-block8__title">装置除去・メンテナンス</h3>
				<p class="c-block8__time">約1時間（装置除去）・約30分（メンテナンス）</p>
				<p class="c-block8__text">
					矯正装置を取り除いた後、何もしなければ歯はもとの位置に変化していこうとします。矯正治療によって得られたきれいな歯並びを維持するために、後戻り防止装置（リテーナー）を装着しメンテナンスの期間に移行します。<br>
					通院間隔は3～6か月に一度で、歯並び・リテーナーのチェック、クリーニングを行います。
				</p>
			</div>
			<div class="c-block8__image">
				<img src="/assets/img/flow/6.jpg" width="400" height="240" alt="">
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block9 (treatment)</div>
<div class="c-block9">
	<div class="c-block9__inner">
		<div class="c-title2">
			<h2 class="c-title2__text">矯正治療について</h2>
		</div>
		<div class="c-block9__image">
			<img src="/assets/img/treatment/1.jpg" width="980" height="480" alt="" class="pc-only">
			<img src="/assets/img/treatment/1_sp.jpg" width="690" height="390" alt="" class="sp-only">
		</div>
		<div class="c-block9__text">
			<p>
				みなさんご存知のように医療技術や材料は日進月歩で進化しています。歯科医療においてもそれは変わりません。<br>
				ただ残念ながら今のところ、自分の歯に勝る材料はありません。矯正治療は自分の歯を大切にすることから始まります。<br>
				歯並びを良くすることは、見た目を良くすることで社会生活を明るくし、噛み合わせからおこる可能性のある健康被害を予防し、ご自身によるお口の中の管理をしやすくすることで虫歯や歯周病といった歯をなくす病気のリスクを下げるというメリットがあります。
			</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block10 (treatment)</div>
<div class="c-block10">
	<div class="c-block10__inner">
		<div class="c-title2">
			<h2 class="c-title2__text">子供の矯正治療<br class="sp-only">（乳歯列期・混合歯列期の治療）</h2>
		</div>
		<div class="c-block10__info">
			<div class="c-block10__text">
				<p>
					早期治療、準備矯正、1期治療などと呼ばれる治療です。<br>
					多くの悪い歯並びは、歯と顎の大きさや位置のアンバランスによって引き起こされます。このアンバランスを、成長を利用することで取り除くことが目的となります。<br>
					平均して6歳から10歳くらいから始めます（受け口の場合は就学前の場合あり）。この時期の治療はしっかりとした歯並びを作るというよりも、これから生え揃う永久歯のために良い環境を作ることが重要です。（家を建てる時の土台作りのイメージです。）この時期から始める理由として大きなものは、治療を受けるお子様の理解力があがり、治療への協力が期待できることです。<br>
					メリットとしては、抜歯治療の可能性を減らすこと、永久歯の治療の期間を短くできる可能性があることです。<br>
					デメリットは、永久歯の治療から始めるよりも治療期間が若干伸びることです。<br>
					近年、この治療から選択される方が増えています。特に子供の矯正治療は十人十色で決まった治療方針があるわけではありません。（場合によっては永久歯が生えそろうまで経過観察することもあります。）<br>
					不安を感じたら現状を把握するためにも、お気軽にご相談ください。
				</p>
			</div>
			<div class="c-block10__image">
				<img src="/assets/img/treatment/2.jpg" width="400" height="400" alt="">
			</div>
		</div>
	</div>
</div>

<div class="c-block10 c-block10--reverse">
	<div class="c-block10__inner">
		<div class="c-title2">
			<h2 class="c-title2__text">大人の矯正治療<br class="sp-only">（永久歯列期の治療）</h2>
		</div>
		<div class="c-block10__info">
			<div class="c-block10__image">
				<img src="/assets/img/treatment/3.jpg" width="520" height="420" alt="">
			</div>
			<div class="c-block10__text">
				<p>
					本格矯正、成人矯正、2期治療（子供の矯正治療を行っている場合）と呼ばれる永久歯の治療です。<br>
					乳歯がすべて交換して永久歯がほぼ生えそろった状態で治療をおこないます。（いよいよ家を建てる時期です。）早い方で小学校高学年から大人と変わらない治療が始められます。歯肉、顎の骨が健康であれば年齢制限はありません。お子様の通院と同時にご自身の治療も始められる親御さんも多くいらっしゃいます。<br>
					10代後半になりますと成長が利用できなくなりますので、症状によって抜歯の有無を選択する必要があります。<br>
					我々矯正科医は決して抜歯が好きなわけではありません。利点・欠点を考慮した上で抜歯の有無をご提案するだけで、ご自身に選択権があります。<br>
					また現在は、昔と違ってライフスタイルに合わせた装置の選択が可能です。表側につけるワイヤー装置・見えない裏側のワイヤー装置・取り外しの可能なマウスピース型の装置の選択ができます。どの装置を選択されても治療を受ける方の協力なしでは、いい結果に結びつきません。<br>
					大人の矯正治療を行う時期は、受験、部活、就職、結婚、転勤など人生の分岐点に立たされる時期でもあります。<br>
					それぞれの患者様にあった治療のタイミングを一緒に考えさせてください。
				</p>
			</div>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-block11 (treatment)</div>
<div class="c-block11">
	<div class="c-block11__inner">
		<div class="c-title2">
			<h2 class="c-title2__text">部分矯正治療</h2>
		</div>
		<div class="c-block11__text">
			<p>
				永久歯列期で、気になる数本の歯だけに限局して行う治療です。<br>
				部分的に行うのでコストが低くなります。<br>
				ただし全体的なバランスをとらないため人によっては、並びは良くなっても位置関係のズレが治らない、逆に出っ歯や受け口ががひどくなるなどの可能性があります。<br>
				メリット・デメリットをお話させていただきますので、その上で治療を受けられることをお勧めします。
			</p>
		</div>
	</div>
</div>

<div class="c-block11 c-block11--bg1">
	<div class="c-block11__inner">
		<div class="c-title2">
			<h2 class="c-title2__text">MFT（口腔筋機能療法）</h2>
		</div>
		<div class="c-block11__text">
			<p>
				歯並びは、お口の周りの筋肉や舌、呼吸と密接な関係があります。<br>
				どれか一つでもバランスが崩れている場合、歯並びが悪くなる原因となります。<br>
				また指しゃぶりや爪噛みなどの癖も歯並びが悪くなる一因です。<br>
				治療が終わって綺麗な歯並びを獲得してもこれらの要因が残っていては、悪い歯並びに戻ってしまう可能性があります。このような歯並びを悪くする要因を取り除くためのトレーニングをMFT（口腔筋機能療法）といいます。（簡単に言うとお口の周りの筋肉や舌の筋トレです。）<br>
				当院でもこのトレーニングが必要と診断された方は、矯正治療前、治療中に関わらず積極的に指導させていただきます。
			</p>
		</div>
	</div>
</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-sideFrame1 (side news)</div>

<div class="c-sideFrame1">
	<div class="c-sideFrame1__head">
		<p>カテゴリー</p>
	</div>
	<div class="c-sideFrame1__body">
		<ul>
			<li>
				<a href="">休診のお知らせ</a>
			</li>
			<li>
				<a href="">コラム</a>
			</li>
			<li>
				<a href="">その他</a>
			</li>
		</ul>
	</div>
</div>