<?php /*========================================
table
================================================*/ ?>
<div class="c-dev-title1">table</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-table1 (cost)</div>
<div class="l-container">
	<div class="c-table1">
		<table>
			<tr>
				<td colspan="3" class="title">初診相談</td>
				<td>無料</td>
			</tr>
			<tr class="bg">
				<td colspan="3" class="title">検査・診断</td>
				<td>¥35,000</td>
			</tr>
			<tr>
				<td rowspan="4" class="title title2">子供の矯正治療</td>
				<td colspan="2">1期治療（乳歯から永久歯へ交換するまでの治療）</td>
				<td>¥400,000</td>
			</tr>
			<tr>
				<td rowspan="3" class="title3">2期治療<br class="pc-only">（1期治療より引き続いた永久歯の治療）</td>
				<td class="title3_item">表側の装置の使用（マルチブラケット）</td>
				<td>¥380,000</td>
			</tr>
			<tr>
				<td class="title3_item">マウスピース型装置の使用（インビザライン）</td>
				<td>¥500,000</td>
			</tr>
			<tr>
				<td class="title3_item">裏側の装置の使用（リンガルブラケット）</td>
				<td>¥950,000</td>
			</tr>
			<tr class="bg">
				<td rowspan="3" colspan="2" class="title">大人の矯正治療（本格矯正治療・永久歯の治療）</td>
				<td>表側の装置の使用（マルチブラケット）</td>
				<td>¥780,000</td>
			</tr>
			<tr class="bg">
				<td>マウスピース型装置の使用（インビザライン）</td>
				<td>¥900,000</td>
			</tr>
			<tr class="bg">
				<td>裏側の装置の使用（リンガルブラケット）</td>
				<td>¥1,350,000</td>
			</tr>
			<tr>
				<td colspan="3" class="title">部分矯正治療</td>
				<td>¥250,000</td>
			</tr>
			<tr class="bg">
				<td colspan="3" class="title">アンカースクリュウ埋入</td>
				<td>¥20,000（1本）</td>
			</tr>
			<tr>
				<td colspan="3" class="title">治療契約前の経過観察（治療契約後払い込み分を治療費から差し引きます）</td>
				<td>¥3,000（1本）</td>
			</tr>
			<tr class="bg">
				<td colspan="3" class="title">
					矯正治療終了後2年以降のメンテナンス（半年から1年に1回程度）<br>及び部分矯正治療、1期治療のみで終了した後のメンテナンス
				</td>
				<td>¥5,000（1回）</td>
			</tr>
			<tr>
				<td rowspan="2" colspan="2" class="title">ホワイトニング</td>
				<td>オフィスホワイトニング：上下16本</td>
				<td>¥15,000</td>
			</tr>
			<tr>
				<td>ホームホワイトニング：約2週間分</td>
				<td>¥30,000<br class="pc-only">（追加薬液¥5,000）</td>
			</tr>
		</table>
	</div>
</div>