<footer class="c-footer">
	<div class="c-footer__innersp sp-only">
		<div class="c-footer__title">
			<p>初診相談は無料で受付しております。<br>ぜひご相談ください。</p>
		</div>
		<p class="c-footer__text2">お電話でのご予約・お問い合わせ</p>
		<div class="c-footer__phone">
			<a href="tel:045-947-3344">045-947-3344</a>
		</div>
		<p class="c-footer__text2">24時間いつでも予約</p>
		<div class="c-btn1">
			<a href="">インターネット予約	</a>
		</div>
	</div>
	<div class="c-footer__toTop sp-only">
		<a href="">ページトップ</a>
	</div>
	<div class="c-footer__inner">
		<div class="c-footer__logo">
			<a href="/">
				<img src="/assets/img/common/logo2.png" width="240" height="34" alt="">
			</a>
		</div>
		<div class="c-footer__text">
			<p>〒221-0002 神奈川県横浜市神奈川区大口通35-33<br>Tel. 045-947-3344</p>
			<p>平日　12:00-20:00（10:00から電話受付開始）<br class="sp-only"> <span class="pc-only">/</span> 土・日　09:30-13:00／14:30 -19:00<br>休診日 木曜・祝日・隔週日曜<br class="sp-only">（日曜診療の場合は翌月曜休診）</p>
		</div>
		<div class="c-footer__link">
			<a href="#">個人情報について</a>
		</div>
		<div class="c-footer__copyright">
			<p>© 2016 ICHINOSE ORTHODONTIC CLINIC</p>
		</div>
	</div>
	<div class="c-top">
		<a></a>
	</div>
</footer>

<script src="/assets/js/slick/slick.min.js"></script>
<script src="/assets/js/functions.js"></script>
</body>
</html>